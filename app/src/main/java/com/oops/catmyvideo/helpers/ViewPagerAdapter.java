package com.oops.catmyvideo.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.oops.catmyvideo.ItemFragment;
import com.oops.catmyvideo.model.Item;

import java.util.Iterator;
import java.util.List;

import lombok.Getter;

/**
 * Displays the description of videos on the view pager.
 */
@Getter
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private List<Item> mItems;
    private FragmentManager mFragmentManager;

    public ViewPagerAdapter(FragmentManager fm, List<Item> items) {
        super(fm);
        mFragmentManager = fm;
        setMItems(items);
    }

    @Override
    public Fragment getItem(int position) {
        return ItemFragment.newInstance(mItems.get(position));
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mItems.get(position).getSnippet().getTitle();
    }

    /**
     * Sets a new list of items to display and updates the fragments accordingly.
     * @param items new list of items
     */
    public void setMItems(List<Item> items) {
        this.mItems = items;
        if (mFragmentManager.getFragments() != null) {
            int i = 0;
            Iterator<Fragment> it = mFragmentManager.getFragments().iterator();
            while (i < items.size() && it.hasNext()) {
                Fragment fragment = it.next();

                if (fragment instanceof ItemFragment) {
                    ((ItemFragment) fragment).setMItem(items.get(i));
                    ++i;
                }
            }
        }
    }
}
