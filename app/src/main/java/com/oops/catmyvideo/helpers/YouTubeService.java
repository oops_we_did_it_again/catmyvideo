package com.oops.catmyvideo.helpers;

import com.oops.catmyvideo.model.ItemRetroFit;

import retrofit.http.GET;

/**
 * Service that retrieves an ItemRetroFit object from the JSON.
 */
public interface YouTubeService {
    String ENDPOINT = "http://tutos-android.com/MTI/2016/Projet";

    @GET("/search.json")
    ItemRetroFit listVideos();
}
