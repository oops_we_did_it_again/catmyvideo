package com.oops.catmyvideo.helpers;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.support.v7.graphics.Palette;

import java.util.HashMap;

/**
 * Handles image and swatch caching. Implements the *Singleton* design pattern.
 */
public class CacheManager {
    private static CacheManager sInstance;

    private LruCache<String, Bitmap> mMemoryCache;
    private HashMap<String, Palette.Swatch> mSwatches;

    /**
     * Creates an instance of the CacheManager class if there was not any and returns this latter.
     * @return static instance of CacheManager
     */
    public static CacheManager getInstance() {
        if (sInstance == null)
            sInstance = new CacheManager();
        return sInstance;
    }

    /**
     * Default constructor. It has a private visibility in order to comply with the singleton
     * design pattern.
     */
    private CacheManager() {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
        mSwatches = new HashMap<>();
    }

    /**
     * Add a downloaded bitmap to the LruCache, associated with its key (in our case, the video's
     * id). Also computes the palette associated to the bitmap and stores the result into a HashMap.
     * Here, we are looking for the Vibrant Swatch of the generated palette.
     * @param key key used to identify the bitmap and the swatch
     * @param bitmap bitmap to store and from which the swatch is generated
     */
    public void addBitmapToMemoryCache(final String key, Bitmap bitmap) {
        // If there is already a bitmap associated to the key, skip this.
        if (getBitmapFromMemCache(key) == null) {
            // Insert bitmap into LruCache.
            mMemoryCache.put(key, bitmap);

            // Generate palette associated and insert it into the HashMap.
            Palette.PaletteAsyncListener paletteListener = new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    if (palette.getVibrantSwatch() != null) {
                        mSwatches.put(key, palette.getVibrantSwatch());
                    }
                }
            };
            new Palette.Builder(bitmap).generate(paletteListener);
        }
    }

    /**
     * Returns the bitmap associated to the key given in parameter.
     * @param key key used to identify the bitmap
     * @return bitmap corresponding to the key if it exists, null otherwise.
     */
    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    /**
     * Returns the swatch associated to the key given in parameter.
     * @param key key used to identify the swatch
     * @return swatch corresponding to the key if it exists, null otherwise.
     */
    public Palette.Swatch getSwatch(String key) {
        return mSwatches.get(key);
    }
}
