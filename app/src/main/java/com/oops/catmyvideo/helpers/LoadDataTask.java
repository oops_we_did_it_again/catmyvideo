package com.oops.catmyvideo.helpers;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;

import com.oops.catmyvideo.model.Item;
import com.oops.catmyvideo.model.ItemRetroFit;
import com.oops.catmyvideo.model.Thumbnails;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import retrofit.RestAdapter;


/**
 * Loads data (JSON and Pictures) from the service.
 */
public class LoadDataTask extends AsyncTask<Void, Integer, ItemRetroFit> {

    private ProgressBar mProgressBar;
    private DisplayMetrics mDisplayMetrics;

    public LoadDataTask(@Nullable ProgressBar progressBar, DisplayMetrics displayMetrics) {
        super();
        mProgressBar = progressBar;
        mDisplayMetrics = displayMetrics;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (mProgressBar != null)
            mProgressBar.setProgress(0);
    }

    @Override
    protected ItemRetroFit doInBackground(Void... arg0) {
        // Build the service.
        YouTubeService youTubeService = new RestAdapter.Builder()
                .setEndpoint(YouTubeService.ENDPOINT)
                .build()
                .create(YouTubeService.class);

        ItemRetroFit itemRetroFit;

        try {
            // Fetch JSON.
            itemRetroFit = youTubeService.listVideos();
        } catch (Exception e) {
            if (e != null && e.getMessage() != null && !e.getMessage().isEmpty())
                Log.e("LOADDATA", e.getMessage());
            else
                Log.e("LOADDATA", "Unknown error :(");
            this.cancel(true);
            return null;
        }

        // Caching pictures.
        Iterator<Item> it = itemRetroFit.getItems().iterator();
        while (it.hasNext()) {

            Item item = it.next();
            if (!item.getId().getKind().equals("youtube#video")) {
                it.remove();
                continue;
            }

            itemRetroFit.incrVideoCount();

            // Select the right picture to cache depending on the device' screen density.
            Thumbnails tn = item.getSnippet().getThumbnails();
            String tnUrl = tn.getDefaultt().getUrl();
            if (mDisplayMetrics.densityDpi >= DisplayMetrics.DENSITY_HIGH)
                tnUrl = tn.getHigh().getUrl();
            else if (mDisplayMetrics.densityDpi >= DisplayMetrics.DENSITY_MEDIUM)
                tnUrl = tn.getMedium().getUrl();

            try {
                // Get URL passed by argument and set open stream.
                URL url = new URL(tnUrl);
                InputStream in = url.openStream();
                BufferedInputStream buf = new BufferedInputStream(in);

                // Extract Bitmap object and close streams.
                Bitmap bMap = BitmapFactory.decodeStream(buf);
                in.close();
                buf.close();

                CacheManager.getInstance().addBitmapToMemoryCache(item.getId().getVideoId(), bMap);

            } catch (Exception e) {
                Log.e("Error reading file", e.toString());
            }

            publishProgress();
        }

        Collections.sort(itemRetroFit.getItems(), new Comparator<Item>() {
                    @Override
                    public int compare(Item lhs, Item rhs) {
                        return lhs.getSnippet().getPublishedAt().compareTo(rhs.getSnippet().getPublishedAt());
                    }
                }
        );
        return itemRetroFit;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (mProgressBar != null) {
            mProgressBar.incrementProgressBy(1);
        }
    }
}
