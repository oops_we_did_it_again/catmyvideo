package com.oops.catmyvideo.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.oops.catmyvideo.R;
import com.oops.catmyvideo.model.Item;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * On the HomeActivity, the CardViews are handled thanks to this adapter.
 */
@Getter
@Setter
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder> {

    private List<Item> mItems;
    private ClickListener mClickListener;
    private int mSelectedItemPosition;
    private Context mContext;

    public ItemAdapter(List<Item> items, ClickListener clickListener, Context context) {
        super();
        mItems = items;
        mClickListener = clickListener;
        mContext = context;
        mSelectedItemPosition = -1; // When mSelectedItemPosition is set to -1, no item is selected.
    }

    @Override
    public ItemAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item, parent, false);
        return new ViewHolder(view, mClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            Item item = mItems.get(position);

            // Set the title, the channel title, the publish date and the thumbnail.
            holder.mTitle.setText(item.getSnippet().getTitle());
            holder.mChannel.setText(item.getSnippet().getChannelTitle());

            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(item.getSnippet().getPublishedAt());
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy, hh:mm a").format(date);
            holder.mPublicationDate.setText(formattedDate);

            Bitmap pic = CacheManager.getInstance().getBitmapFromMemCache(item.getId().getVideoId());

            if (pic != null) {
                holder.mImageView.setImageBitmap(pic);
                holder.mImageView.setVisibility(View.VISIBLE);
            } else {
                holder.mImageView.setVisibility(View.INVISIBLE);
            }

            // Change the appearance of the CardView, whether it is selected or not.
            if (position == mSelectedItemPosition) {
                Palette.Swatch swatch = CacheManager.getInstance().getSwatch(item.getId().getVideoId());
                if (swatch == null) {
                    // Default layout (selected).
                    holder.mLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryDark));
                    holder.mTitle.setTextColor(Color.WHITE);
                    holder.mChannel.setTextColor(ContextCompat.getColor(mContext, R.color.subtitle));
                    holder.mPublicationDate.setTextColor(ContextCompat.getColor(mContext, R.color.subtitle));
                }
                else {
                    // Custom Swatch layout (selected).
                    holder.mLayout.setBackgroundColor(swatch.getRgb());
                    holder.mTitle.setTextColor(swatch.getTitleTextColor());
                    holder.mChannel.setTextColor(swatch.getBodyTextColor());
                    holder.mPublicationDate.setTextColor(swatch.getBodyTextColor());
                }
            } else {
                // Default layout (non-selected).
                holder.mLayout.setBackgroundColor(Color.WHITE);
                holder.mTitle.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                holder.mChannel.setTextColor(ContextCompat.getColor(mContext, R.color.subtitle));
                holder.mPublicationDate.setTextColor(ContextCompat.getColor(mContext, R.color.subtitle));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface ClickListener {
        void OnClick(View v, int position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        public RelativeLayout mLayout;
        public TextView mTitle;
        public TextView mChannel;
        public TextView mPublicationDate;
        public ImageView mImageView;

        public ViewHolder(View v, final ClickListener clickListener) {
            super(v);

            mLayout = (RelativeLayout) v.findViewById(R.id.relativeLayout);
            mTitle = (TextView) v.findViewById(R.id.video_title);
            mChannel = (TextView) v.findViewById(R.id.video_channel);
            mPublicationDate = (TextView) v.findViewById(R.id.publication_date);

            mImageView = (ImageView) v.findViewById(R.id.img);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Specific callback in order to get cardview_item's position
                    clickListener.OnClick(v, getAdapterPosition());
                }
            });
        }
    }
}

