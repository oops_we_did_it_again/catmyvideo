package com.oops.catmyvideo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.oops.catmyvideo.helpers.CustomViewPager;
import com.oops.catmyvideo.helpers.ViewPagerAdapter;
import com.oops.catmyvideo.model.Item;
import com.oops.catmyvideo.model.ItemRetroFit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * Main activity of the application. Changes according to the current orientation.
 * - Portrait : displays a list of videos.
 * - Landscape : displays two fragments, one with the list of videos and the other with the detailed
 * page of the video.
 */
@Getter
@Setter
public class HomeActivity extends AppCompatActivity implements View.OnClickListener, SearchView.OnQueryTextListener {

    private Toolbar mToolbar;
    private ItemRetroFit mItemRetroFit;
    private List<Item> mSearchResultItems;
    private List<OnItemsChangedListener> mOnItemsChangedListeners;
    private ViewPagerFragment mViewPagerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get fetched items list.
        mItemRetroFit = getIntent().getExtras().getParcelable("item_retro_fit");

        // Fragments list to notify on search update.
        mOnItemsChangedListeners = new ArrayList<>();

        if (savedInstanceState != null) {
            // Feed resulting items into search ones.
            mSearchResultItems = new ArrayList<>();
            for (Item item : mItemRetroFit.getItems()) {
                Serializable s = savedInstanceState.getSerializable(item.getEtag());
                if (s != null)
                    mSearchResultItems.add((Item) s);
            }
        }
        initUI();
        loadComponents();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mToolbar.getChildAt(1).getId()) {
            // Display AUTHORS dialog if we clicked on the logo.
            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setTitle(R.string.about_catmyvideo)
                    .setMessage(Html.fromHtml(getString(R.string.authors)))
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Do nothing.
                        }
                    })
                    .create().show();
        } else if (v.getId() == R.id.share_button) {
            // Prepare sharing intent if we clicked on the "Share" icon.
            ViewPagerAdapter adapter = mViewPagerFragment.getAdapter();
            if (mViewPagerFragment != null && adapter.getCount() != 0) {
                // Inform user that the action has been taken into account.
                Toast.makeText(this, "Prepare sharing...", Toast.LENGTH_SHORT).show();


                // Fetch current item.
                CustomViewPager viewPager = mViewPagerFragment.getViewPager();
                Item item = ((ItemFragment) adapter.getItem(viewPager.getCurrentItem())).getMItem();

                // Create share intent.
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, item.getVideoLink());
                intent.putExtra(android.content.Intent.EXTRA_SUBJECT, item.getSnippet().getTitle());

                // Launch intent.
                startActivity(Intent.createChooser(intent, "Share"));
            } else {
                Toast.makeText(this, "Nothing to share.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void initUI() {
        // Set layout to inflate as well as in and out transition.
        setContentView(R.layout.activity_home);
        overridePendingTransition(R.anim.push_up_in, R.anim.push_up_out);

        // Toolbar settings: set as ActionBar, hide the title, set the logo and make it clickable.
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        mToolbar.setLogo(R.mipmap.ic_launcher);
        View logo = mToolbar.getChildAt(1);
        logo.setOnClickListener(this);

        // Set "Share" button listener.
        ImageButton shareButton = (ImageButton) findViewById(R.id.share_button);
        if (shareButton != null)
            shareButton.setOnClickListener(this);

    }

    private void loadComponents() {

        // Prepare bundle.
        Bundle bundle = new Bundle();
        bundle.putParcelable("item_retro_fit", mItemRetroFit);

        FragmentManager fragmentManager = getSupportFragmentManager();

        // Init once an instance of ItemsListFragment.
        ItemsListFragment itemsListFragment = (ItemsListFragment) fragmentManager.findFragmentById(R.id.item_list_fragment);
        if (itemsListFragment == null) {
            itemsListFragment = ItemsListFragment.newInstance();
            itemsListFragment.setArguments(bundle);
            fragmentManager.beginTransaction().add(R.id.item_list_fragment, itemsListFragment).commit();
        }

        mOnItemsChangedListeners.add(itemsListFragment);

        // Init SearchView component callbacks.
        SearchView searchView = (SearchView) findViewById(R.id.search_view);
        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
        }

        // Init once an instance of ViewPagerFragment in landscape
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mViewPagerFragment = (ViewPagerFragment) fragmentManager.findFragmentById(R.id.view_pager_fragment);
            if (mViewPagerFragment == null) {
                mViewPagerFragment = new ViewPagerFragment();
                bundle.putInt("current_item", 0);
                mViewPagerFragment.setArguments(bundle);
                fragmentManager.beginTransaction().add(R.id.view_pager_fragment, mViewPagerFragment).commit();
            }

            mOnItemsChangedListeners.add(mViewPagerFragment);
            if (mSearchResultItems != null && !mSearchResultItems.isEmpty())
                mViewPagerFragment.onItemsChanged(mSearchResultItems);
        }
    }

    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        if (mItemRetroFit != null) {
            if (newText.equals("")) {
                // If the search query is erased or just blank: display the list of all videos.
                showNoResultsView(false);
                for (OnItemsChangedListener listener : mOnItemsChangedListeners)
                    listener.onItemsChanged(mItemRetroFit.getItems());
            } else {
                // Otherwise, filter only the matching videos.
                List<Item> tmpList = new ArrayList<>(mItemRetroFit.getItems());

                Iterator<Item> it = tmpList.iterator();
                String[] toMatch = newText.toLowerCase().split("\\s+");
                while (it.hasNext()) {
                    Item item = it.next();
                    for (String text : toMatch) {
                        if (!item.getSnippet().getTitle().toLowerCase().contains(text) &&
                                !item.getSnippet().getDescription().toLowerCase().contains(text) &&
                                !item.getSnippet().getChannelTitle().toLowerCase().contains(text)) {
                            it.remove();
                            break;
                        }
                    }
                }

                // Show the "No results found." view if no video matches the query.
                showNoResultsView(tmpList.isEmpty());

                // Notify listeners that the list of videos has been updated.
                for (OnItemsChangedListener listener : mOnItemsChangedListeners)
                    listener.onItemsChanged(tmpList);
                mSearchResultItems = tmpList;
            }
        }
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mSearchResultItems != null)
            for (Item item : mSearchResultItems)
                outState.putSerializable(item.getEtag(), item);
    }

    public interface OnItemsChangedListener {
        void onItemsChanged(List<Item> items);
    }

    private void showNoResultsView(boolean show) {
        LinearLayout content = (LinearLayout) findViewById(R.id.contentLayout);
        LinearLayout noResults = (LinearLayout) findViewById(R.id.noResultsLayout);

        if (!show) {
            content.setVisibility(View.VISIBLE);
            noResults.setVisibility(View.GONE);
        } else {
            content.setVisibility(View.GONE);
            noResults.setVisibility(View.VISIBLE);
        }
    }
}
