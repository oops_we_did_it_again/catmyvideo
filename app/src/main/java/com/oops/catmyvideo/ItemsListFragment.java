package com.oops.catmyvideo;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PointF;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.oops.catmyvideo.helpers.ItemAdapter;
import com.oops.catmyvideo.helpers.LoadDataTask;
import com.oops.catmyvideo.model.Item;
import com.oops.catmyvideo.model.ItemRetroFit;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Fragment that holds the list of videos.
 */
public class ItemsListFragment extends Fragment implements ItemAdapter.ClickListener, HomeActivity.OnItemsChangedListener, SwipeRefreshLayout.OnRefreshListener {

    private ItemRetroFit mItemRetroFit;
    private ItemAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private LinearLayoutManager mLayoutManager;

    public static ItemsListFragment newInstance() {
        return new ItemsListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate list layout.
        final View view = inflater.inflate(R.layout.fragment_items_list, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);

        return view;
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mItemRetroFit = getArguments().getParcelable("item_retro_fit");

        mLayoutManager = new SnappingLinearLayoutManager(getActivity().getApplicationContext());
        DisplayMetrics dm = getActivity().getResources().getDisplayMetrics();
        mAdapter = new ItemAdapter(mItemRetroFit.getItems(), this, getContext());

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    // Click listener on list items
    @Override
    public void OnClick(View v, int position) {
        if (getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {

            // Display clicked item.
            ViewPagerFragment frg = (ViewPagerFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.view_pager_fragment);
            frg.getViewPager().setCurrentItem(position);
            ((ScrollView) frg.getView().getParent()).fullScroll(ScrollView.FOCUS_UP);

            // Inform the adapter this item has been selected.
            mAdapter.setMSelectedItemPosition(position);
            mAdapter.notifyDataSetChanged();

            // Scroll to the top.
            mRecyclerView.smoothScrollToPosition(position);
        } else {
            // Go to the detailed view.
            Intent intent = new Intent(getActivity(), ViewPagerActivity.class);
            ItemRetroFit itemRetroFit = new ItemRetroFit();
            itemRetroFit.setEtag(mItemRetroFit.getEtag());
            itemRetroFit.setKind(mItemRetroFit.getKind());
            itemRetroFit.setNextPageToken(mItemRetroFit.getNextPageToken());
            itemRetroFit.setPageInfo(mItemRetroFit.getPageInfo());
            itemRetroFit.setVideoCount(mAdapter.getItemCount());
            itemRetroFit.setItems(mAdapter.getMItems());

            intent.putExtra("item_retro_fit", itemRetroFit);
            intent.putExtra("current_item", position);
            startActivity(intent);
        }
    }

    @Override
    public void onItemsChanged (final List<Item> items) {
        mAdapter.setMItems(items);
        mAdapter.setMSelectedItemPosition(-1);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onRefresh () {
        mSwipeRefreshLayout.setRefreshing(true);
        if (getActivity() instanceof HomeActivity) {
            final LoadDataTask task = new LoadDataTask(null, getActivity().getResources().getDisplayMetrics());
            task.execute();

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run () {
                    try {
                        ItemRetroFit itemRetroFit = task.get();
                        if (itemRetroFit != null)
                            ((HomeActivity) getActivity()).setMItemRetroFit(itemRetroFit);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        mSwipeRefreshLayout.post(new Runnable() {
                            @Override
                            public void run() {
                                mSwipeRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }
            });

        }
    }

    // LayoutManager to scroll on item selection
    class SnappingLinearLayoutManager extends LinearLayoutManager {

        public SnappingLinearLayoutManager(Context context) {
            super(context);
        }

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state,
                                           int position) {
            RecyclerView.SmoothScroller smoothScroller = new TopSnappedSmoothScroller(recyclerView.getContext());
            smoothScroller.setTargetPosition(position);
            startSmoothScroll(smoothScroller);
        }

        private class TopSnappedSmoothScroller extends LinearSmoothScroller {
            public TopSnappedSmoothScroller(Context context) {
                super(context);

            }

            @Override
            public PointF computeScrollVectorForPosition(int targetPosition) {
                return SnappingLinearLayoutManager.this
                        .computeScrollVectorForPosition(targetPosition);
            }

            @Override
            protected int getVerticalSnapPreference() {
                return SNAP_TO_START;
            }
        }
    }
}
