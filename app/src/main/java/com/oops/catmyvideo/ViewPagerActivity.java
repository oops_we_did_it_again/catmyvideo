package com.oops.catmyvideo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.oops.catmyvideo.helpers.CustomViewPager;
import com.oops.catmyvideo.helpers.ViewPagerAdapter;
import com.oops.catmyvideo.model.Item;
import com.oops.catmyvideo.model.ItemRetroFit;

/**
 * ViewPagerActivity.
 * When the user clicks on an item on the HomeActivity list, he is redirected on this activity.
 * Handles the ViewPager components for our videos.
 */
public class ViewPagerActivity extends AppCompatActivity implements View.OnClickListener
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set layout to inflate as well as in and out transition.
        setContentView(R.layout.activity_view_pager);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);

        // Share button listener
        findViewById(R.id.share_button).setOnClickListener(this);

        // Get videos.
        ItemRetroFit itemRetroFit = getIntent().getParcelableExtra("item_retro_fit");
        int currentItem = getIntent().getIntExtra("current_item", 0);

        // Display toolbar.
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null)
            setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        // Inflate ViewPagerFragment.
        FragmentManager fragmentManager = getSupportFragmentManager();
        ViewPagerFragment viewPagerFragment = (ViewPagerFragment) fragmentManager.findFragmentById(R.id.view_pager_fragment);

        if (viewPagerFragment == null) {
            viewPagerFragment = new ViewPagerFragment();
            Bundle args = new Bundle();
            args.putParcelable("item_retro_fit", itemRetroFit);
            args.putInt("current_item", currentItem);
            viewPagerFragment.setArguments(args);
            fragmentManager.beginTransaction().add(R.id.view_pager_fragment, viewPagerFragment).commit();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.push_right_in, R.anim.push_right_out);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.share_button) {
            // Inform user that the action has been taken into account.
            Toast.makeText(this, "Prepare sharing...", Toast.LENGTH_SHORT).show();

            // Get ViewPager and its adapter from the fragment.
            FragmentManager fragmentManager = getSupportFragmentManager();
            ViewPagerFragment fragment = (ViewPagerFragment) fragmentManager.findFragmentById(R.id.view_pager_fragment);
            ViewPagerAdapter mAdapter = fragment.getAdapter();
            CustomViewPager mViewPager = fragment.getViewPager();

            // Create share intent.
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");

            Item item = ((ItemFragment) mAdapter.getItem(mViewPager.getCurrentItem())).getMItem();

            intent.putExtra(Intent.EXTRA_TEXT, item.getVideoLink());
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, item.getSnippet().getTitle());
            startActivity(Intent.createChooser(intent, "Share"));
        }
    }
}
