package com.oops.catmyvideo;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.oops.catmyvideo.helpers.LoadDataTask;
import com.oops.catmyvideo.model.ItemRetroFit;

/**
 * Displays the splash screen and loads the JSON file and each of its items.
 * If no connection is detected, displays a message and a retry button.
 */
public class SplashScreenActivity extends AppCompatActivity implements View.OnClickListener {

    private View mErrorContainer;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        mErrorContainer = findViewById(R.id.error_container);
        findViewById(R.id.button_retry).setOnClickListener(this);

        // Init progress bar.
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.setIndeterminate(false);
        mProgressBar.setMax(27);

        loadData();
    }

    private void loadData() {
        // Start data loading.
        try {
            final AsyncTask<Void, Integer, ItemRetroFit> loadTask = new LoadDataTask(mProgressBar, getResources().getDisplayMetrics()).execute();
            // Display the screen during 1 second, at least.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run () {
                    Intent i = new Intent(SplashScreenActivity.this, HomeActivity.class);

                    try {
                        final ItemRetroFit finalItemRetroFit = loadTask.get();
                        if (finalItemRetroFit == null)
                            throw new Exception("Can't fetch JSON data");
                        i.putExtra("item_retro_fit", finalItemRetroFit);
                        startActivity(i);
                        finish();
                    } catch (Exception e) {
                        Log.e("SPLASHSCREEN", e.toString());
                        displayError();
                    }

                }
            }, 1000);
        } catch (Exception e) {
            Log.e("SPLASHSCREEN", e.getMessage());
            displayError();
        }
    }

    private void displayError() {
        mErrorContainer.setVisibility(View.VISIBLE);
    }

    private void hideError() {
        mErrorContainer.setVisibility(View.GONE);
    }

    @Override
    public void onClick (View v) {
        if (v.getId() == R.id.button_retry) {
            loadData();
            hideError();
        }
    }
}
