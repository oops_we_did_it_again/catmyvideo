package com.oops.catmyvideo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oops.catmyvideo.helpers.CustomViewPager;
import com.oops.catmyvideo.helpers.ViewPagerAdapter;
import com.oops.catmyvideo.model.Item;
import com.oops.catmyvideo.model.ItemRetroFit;

import java.util.List;

/**
 * Fragment that represents a video item. Basically, it displays the video in a player and
 * the associated details (title, description, publish date, etc).
 */
public class ViewPagerFragment extends Fragment implements ViewPager.OnPageChangeListener, HomeActivity.OnItemsChangedListener {

    private YouTubeFragment mPlayer;
    private ViewPagerAdapter mAdapter;
    private CustomViewPager mViewPager;

    private List<Item> mSearchResultItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_pager, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceBundle) {
        super.onViewCreated(view, savedInstanceBundle);

        if (mSearchResultItems == null) {
            // Initialize ViewPager component.
            ItemRetroFit itemRetroFit = getArguments().getParcelable("item_retro_fit");
            mAdapter = new ViewPagerAdapter(getChildFragmentManager(), itemRetroFit.getItems());
        } else {
            mAdapter = new ViewPagerAdapter(getChildFragmentManager(), mSearchResultItems);
        }
        if (savedInstanceBundle == null) {
            // Initialize YouTube player.
            mPlayer = new YouTubeFragment();
            getChildFragmentManager().beginTransaction().replace(R.id.youtube_view, mPlayer).commit();
        } else {
            mPlayer = (YouTubeFragment) getChildFragmentManager().findFragmentById(R.id.youtube_view);
        }

        mViewPager = (CustomViewPager) view.findViewById(R.id.view_pager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.addOnPageChangeListener(this);

        // Make ViewPager display the selected item.
        int currentItem = getArguments().getInt("current_item");
        mViewPager.setCurrentItem(currentItem);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        // Change the video on page scroll.
        if (!mAdapter.getMItems().isEmpty()) {
            String videoId = mAdapter.getMItems().get(position).getId().getVideoId();
            mPlayer.playVideo(videoId);
        }
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public ViewPagerAdapter getAdapter() {
        return mAdapter;
    }

    public CustomViewPager getViewPager() {
        return mViewPager;
    }

    @Override
    public void onItemsChanged(final List<Item> items) {
        if (mAdapter != null && mViewPager != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (items.size() != mAdapter.getMItems().size()) {
                        mAdapter = new ViewPagerAdapter(getChildFragmentManager(), items);
                        mViewPager.setAdapter(mAdapter);
                    } else {
                        mAdapter.setMItems(items);
                    }

                    if (!items.isEmpty()) {
                        mViewPager.setCurrentItem(0);
                        String videoId = mAdapter.getMItems().get(0).getId().getVideoId();
                        mPlayer.playVideo(videoId);
                    }
                }
            });
        }
        mSearchResultItems = items;
    }
}
