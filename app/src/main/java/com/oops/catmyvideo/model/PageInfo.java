package com.oops.catmyvideo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageInfo implements Serializable{
    int totalResults;
    int resultsPerPage;
}
