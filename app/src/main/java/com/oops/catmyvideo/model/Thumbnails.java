package com.oops.catmyvideo.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Thumbnails implements Serializable {
    @SerializedName("default")
    ThumbnailUrl defaultt;
    ThumbnailUrl medium;
    ThumbnailUrl high;
}