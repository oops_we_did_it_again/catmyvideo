package com.oops.catmyvideo.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemRetroFit implements Parcelable {
    String kind;
    String etag;
    String nextPageToken;
    int videoCount;
    PageInfo pageInfo;
    List<Item> items;

    public ItemRetroFit()
    {}

    private ItemRetroFit(Parcel in)
    {
        kind = in.readString();
        etag = in.readString();
        nextPageToken = in.readString();
        videoCount = in.readInt();
        pageInfo = (PageInfo) in.readSerializable();

        items = new ArrayList<>();

        for (int i = 0; i < videoCount; i++) {
            items.add((Item) in.readSerializable());
        }
    }

    public static final Parcelable.Creator<ItemRetroFit> CREATOR = new Parcelable.Creator<ItemRetroFit>()
    {
        @Override
        public ItemRetroFit createFromParcel(Parcel source)
        {
            return new ItemRetroFit(source);
        }

        @Override
        public ItemRetroFit[] newArray(int size)
        {
            return new ItemRetroFit[size];
        }
    };


    @Override
    public int describeContents () {
        return 0;
    }

    @Override
    public void writeToParcel (Parcel dest, int flags) {
        dest.writeString(kind);
        dest.writeString(etag);
        dest.writeString(nextPageToken);
        dest.writeInt(videoCount);
        dest.writeSerializable(pageInfo);

        for (Item item : items) {
            dest.writeSerializable(item);
        }
    }

    public void incrVideoCount () {
        videoCount++;
    }
}
