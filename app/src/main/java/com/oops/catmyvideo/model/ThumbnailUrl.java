package com.oops.catmyvideo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ThumbnailUrl implements Serializable {
    String url;
}
