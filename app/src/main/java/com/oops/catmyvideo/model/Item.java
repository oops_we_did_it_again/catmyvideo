package com.oops.catmyvideo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Item implements Serializable {
    String etag;
    String kind;
    Id id;
    Snippet snippet;

    public String getVideoLink () {
        return "https://www.youtube.com/watch?v=" + id.getVideoId();
    }
}
