package com.oops.catmyvideo.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Snippet implements Serializable {
    String publishedAt;
    String channelId;
    String title;
    String description;
    String channelTitle;
    String liveBroadcastContent;
    Thumbnails thumbnails;
}

