package com.oops.catmyvideo;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

/**
 * YouTube Player for YouTube videos. :)
 */
public class YouTubeFragment extends YouTubePlayerSupportFragment implements YouTubePlayer.OnInitializedListener {

    private static final int RECOVERY_REQUEST = 1;
    private boolean initialized = false;

    private YouTubePlayer.Provider mProvider;
    private YouTubePlayer mActivePlayer;
    private String currentVideoId = "";

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View v = super.onCreateView(layoutInflater, viewGroup, bundle);

        // Initialize the player with the API_KEY and set the Initialization listener
        initialize(getString(R.string.API_KEY), this);

        // Manage reloading
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!initialized)
                    YouTubeFragment.this.initialize(getString(R.string.API_KEY), YouTubeFragment.this);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            mProvider.initialize(getString(R.string.API_KEY), this);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        // Error hanlding
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(getActivity(), RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), errorReason.toString());
            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
        // Keep reference on the player and the provider
        mActivePlayer = player;
        mProvider = provider;

        mActivePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        initialized = true;

        if (!currentVideoId.isEmpty()) {
            mActivePlayer.cueVideo(currentVideoId, 0);
        }
    }

    public void playVideo(String videoId) {
        if (videoId != currentVideoId) {
            currentVideoId = videoId;
            if (initialized) {
                // Load a video
                mActivePlayer.cueVideo(videoId);
            }
        }
    }
}