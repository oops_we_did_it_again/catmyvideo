package com.oops.catmyvideo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oops.catmyvideo.model.Item;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents the fragment used by the view pager to display information about an item.
 */
@Getter
@Setter
public class ItemFragment extends Fragment {

    private Item mItem;

    private TextView mTitle;
    private TextView mChannel;
    private TextView mDesc;
    private TextView mDateView;

    public static ItemFragment newInstance(Item item) {
        ItemFragment fragment = new ItemFragment();
        fragment.mItem = item;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_item, container, false);

        mTitle = (TextView) rootView.findViewById(R.id.video_title);
        mChannel = (TextView) rootView.findViewById(R.id.channel_title);
        mDesc = (TextView) rootView.findViewById(R.id.video_description);
        mDateView = (TextView) rootView.findViewById(R.id.video_date);

        updateDisplayedInformation();

        return rootView;
    }

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mItem = (Item) savedInstanceState.getSerializable("item");
        }
    }

    private void updateDisplayedInformation() {
        if (mTitle == null || mChannel == null || mDesc == null || mDateView == null)
            return;

        // Set video title.
        mTitle.setText(mItem.getSnippet().getTitle());
        // Set channel title.
        if (!mItem.getSnippet().getChannelTitle().isEmpty())
            mChannel.setText("by " + mItem.getSnippet().getChannelTitle());
        // Set video description.
        mDesc.setText(mItem.getSnippet().getDescription());

        try {
            // Set date.
            Date date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(mItem.getSnippet().getPublishedAt());
            String formattedDate = new SimpleDateFormat("dd/MM/yyyy, hh:mm a").format(date);
            mDateView.setText("Publish date: " + formattedDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMItem(Item item) {
        mItem = item;
        updateDisplayedInformation();
    }
    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("item", mItem);
    }
}
